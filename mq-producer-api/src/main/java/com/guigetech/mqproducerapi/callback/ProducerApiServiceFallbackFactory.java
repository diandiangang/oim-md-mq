package com.guigetech.mqproducerapi.callback;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;

import com.guigetech.mqproducerapi.service.ProducerApiService;


/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title: AdminApiServiceFallbackFactory
 * @Prject: guige-admin
 * @Package: com.guige.base.callback
 * @Description:
 * @author: feejson
 * @date: 2022/4/12 10:41 上午
 * @version: V1.0
 */
public class ProducerApiServiceFallbackFactory implements FallbackFactory<ProducerApiService> {
    public static Logger logger = LoggerFactory.getLogger(ProducerApiServiceFallbackFactory.class);
    
    @Override
    public ProducerApiService create(Throwable throwable) {
        logger.error("BaseApiService调用失败：" + throwable.getMessage());
        
        return new ProducerApiService() {
            @Override
            public String sendMsg(String bindingName, String body) {
                logger.error("ProducerApiService-：" + throwable.getMessage());
                StringWriter sw = new StringWriter();
                throwable.printStackTrace(new PrintWriter(sw, true));
                logger.error("ProducerApiServiceFallbackFactory.java(sendMsg) -->" + "[bindingName]" + "   bindingName=" + bindingName + "Exception=" + sw.toString());
                // 降级
                return "调用服务失败,请稍后请求";
            }
        };
    }
}
