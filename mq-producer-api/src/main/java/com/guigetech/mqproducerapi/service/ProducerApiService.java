package com.guigetech.mqproducerapi.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.guigetech.mqproducerapi.callback.ProducerApiServiceFallbackFactory;

@Component
@FeignClient(value = "${com.guige.md.mq}", path = "/mq/rpc", fallbackFactory = ProducerApiServiceFallbackFactory.class)
public interface ProducerApiService {
    
    @GetMapping("/sendMsg")
    public String sendMsg(@RequestParam String bindingName, @RequestParam String body);
}
