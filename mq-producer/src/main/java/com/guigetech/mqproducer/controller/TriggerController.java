package com.guigetech.mqproducer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.guigetech.mqmodel.dto.AccmgrDTO;
import com.guigetech.mqproducer.mq.MsgProducer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by shusheng007
 *
 * @author benwang
 * @date 2022/7/21 1:50 下午
 * @description:
 */

@RequiredArgsConstructor
@RestController
@RequestMapping("/trigger")
@Slf4j
public class TriggerController {
    
    private final MsgProducer msgProducer;
    
    
    @GetMapping("/sendMsg")
    public String sendMsg(@RequestParam String bindingName, @RequestParam String body) {
        AccmgrDTO accmgrDTO = new AccmgrDTO();
        accmgrDTO.setMgrId(0);
        accmgrDTO.setMgrNm("1111");
        accmgrDTO.setTelPhone("2222");
        accmgrDTO.setMPhone("3333");
        accmgrDTO.setMgrEmail("444");
        accmgrDTO.setRemark("5555");
        msgProducer.send(bindingName, accmgrDTO);
        log.info("SendMessage:" + accmgrDTO);
        return "ok";
    }
}
