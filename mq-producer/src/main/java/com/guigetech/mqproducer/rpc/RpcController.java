package com.guigetech.mqproducer.rpc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.guigetech.mqproducer.mq.MsgProducer;

import lombok.RequiredArgsConstructor;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title RpcController
 * @Prject: oim-md-mq
 * @Package: com.guigetech.mqproducer.rpc
 * @Description:
 * @Author: liyl
 * @Date: 2023/3/24 14:00
 * @version:
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/mq/rpc")
public class RpcController {
    
    private final MsgProducer msgProducer;
    
    
    @GetMapping("/sendMsg")
    public String sendMsg(@RequestParam String bindingName, @RequestParam String body) {
        msgProducer.send(bindingName, body);
        return "ok";
    }
}
