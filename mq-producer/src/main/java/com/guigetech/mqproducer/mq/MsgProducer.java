package com.guigetech.mqproducer.mq;

import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/**
 * 生产者
 *
 * @author: lyl
 * @date: 2023/3/22
 */

@RequiredArgsConstructor
@Service
public class MsgProducer {
    
    private final StreamBridge streamBridgeTemplate;
    
    /**
     * 发送消息
     *
     * @param bindingName 主题名称
     * @param body        发送消息体
     */
    public void send(String bindingName, Object body) {
        streamBridgeTemplate.send(bindingName, body);
    }
}
