package com.guigetech.mqmodel.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccmgrDTO {
    
    private Integer mgrId;
    
    private String mgrNm;
    
    private String telPhone;
    
    private String mPhone;
    
    private String mgrEmail;
    
    private String remark;
    
}
