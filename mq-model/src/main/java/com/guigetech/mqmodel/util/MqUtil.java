package com.guigetech.mqmodel.util;

/**
 * Copyright © XXX. All rights reserved.
 *
 * @Title MqUtil
 * @Prject: oim-md-mq
 * @Package: com.guigetech.oimmdmq.util
 * @Description:
 * @Author: liyl
 * @Date: 2023/3/22 15:07
 * @version:
 */
public class MqUtil {
    
    public static final String VALUATION_DATAANALYSIS_TOPIC = "valuation-dataanalysis-topic";
    public static final String TRANS_FUND_DATAPROCESSING_TOPIC = "trans-fund-dataprocessing-topic";
}
