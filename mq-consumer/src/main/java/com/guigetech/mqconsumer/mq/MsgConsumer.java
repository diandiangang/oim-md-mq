package com.guigetech.mqconsumer.mq;

import java.util.function.Consumer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.guigetech.mqmodel.dto.AccmgrDTO;

import lombok.extern.slf4j.Slf4j;

/**
 * 消费者
 *
 * @author: lyl
 * @date: 2023/3/22
 */
@Slf4j
@Configuration
// @RestController
public class MsgConsumer {
    
    
    @Bean
    public Consumer<AccmgrDTO> valuationDataanalysisConsumer() {
        return new Consumer<AccmgrDTO>() {
            @Override
            public void accept(AccmgrDTO s) {
                log.info("valuationDataanalysisConsumer接到消息：{}", s);
            }
        };
    }
    
    @Bean
    public Consumer<String> transFundDataprocessingConsumer() {
        return new Consumer<String>() {
            @Override
            public void accept(String s) {
                log.info("transFundDataprocessingConsumer接到消息：{}", s);
            }
        };
    }
    
}